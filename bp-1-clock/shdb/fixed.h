#pragma once

#include "schema.h"
#include "table.h"

namespace shdb {

PageProvider create_fixed_page_provider(std::shared_ptr<Schema> schema);

}    // namespace shdb
