#include "jit.h"

using dummy_sgn = void(*)();
dummy_sgn answer_dummy(shdb::Jit &jit);

using constant_sgn = int(*)();
constant_sgn answer_constant(shdb::Jit &jit, int constant);

using trivial_sgn = int(*)(int);
trivial_sgn answer_trivial(shdb::Jit &jit);

uintptr_t answer_sum(shdb::Jit &jit, int count);

int helper_call();
using call_sgn = int(*)();
call_sgn answer_call(shdb::Jit &jit);

using abs_sgn = int(*)(int);
abs_sgn answer_abs(shdb::Jit &jit);

int helper_for();
using for_sgn = int(*)(int);
for_sgn answer_for(shdb::Jit &jit);

