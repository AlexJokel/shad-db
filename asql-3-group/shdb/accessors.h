#pragma once

#include "jit.h"
#include "row.h"
#include "schema.h"

namespace shdb {

class Accessors
{
    void link_accessors(Jit &jit);
    void declare_accessors(Jit &jit);

public:
    llvm::Function *rowset_allocate;
    llvm::Function *rowset_get;
    llvm::Function *rowset_size;
    llvm::Function *rowset_sort;

    llvm::Function *hashset_find;
    llvm::Function *hashset_find_or_create_rowset;

    llvm::Function *groupset_find_state;
    llvm::Function *groupset_create_state;
    llvm::Function *groupset_get_state;
    llvm::Function *groupset_get_key;
    llvm::Function *groupset_size;

    llvm::Function *row_load_boolean;
    llvm::Function *row_load_uint64;
    llvm::Function *row_load_varchar;
    llvm::Function *row_load_string;
    llvm::Function *row_load_string_length;

    llvm::Function *row_store_boolean;
    llvm::Function *row_store_uint64;
    llvm::Function *row_store_varchar;
    llvm::Function *row_store_string;

    llvm::Function *compare_strings;

    Accessors(Jit &jit);
};


class RowsetAccessor
{
    Jit &jit;
    std::shared_ptr<Accessors> accessors;
    llvm::Value *rowset;

public:
    RowsetAccessor(Jit &jit, std::shared_ptr<Accessors> accessors, llvm::Value *rowset);

    llvm::Value *allocate_row();
    llvm::Value *get_row(llvm::Value *index);
    llvm::Value *get_rowset_size();
    void sort_rows(llvm::Value *comparer);
};


using JitValue = std::pair<llvm::Value *, llvm::Value *>;
using JitRow = std::vector<JitValue>;

class RowsAccessor
{
    Jit &jit;
    std::shared_ptr<Accessors> accessors;
    std::shared_ptr<Schema> schema;

    JitValue load_value(llvm::Value *row, int index);
    void store_value(const JitRow &jitrow, llvm::Value *row, int index);

public:
    RowsAccessor(Jit &jit, std::shared_ptr<Accessors> accessors, std::shared_ptr<Schema> schema);

    JitRow load_row(llvm::Value *row);
    void store_row(const JitRow &jitrow, llvm::Value *row);
};


class SchemaAccessor
{
    std::shared_ptr<Schema> schema;
    std::unordered_map<std::string, size_t> mapping;

public:
    explicit SchemaAccessor(std::shared_ptr<Schema> schema);

    bool has(const std::string &name);
    size_t get_index(const std::string &name);
    const ColumnSchema &get_column(const std::string &name);
    void add_column(ColumnSchema &&column);
};


class HashsetAccessor
{
    Jit &jit;
    std::shared_ptr<Accessors> accessors;
    llvm::Value *hashset;
    Row tmp;

public:
    HashsetAccessor(Jit &jit, std::shared_ptr<Accessors> accessors, llvm::Value *hashset);

    llvm::Value *find_rowset(llvm::Value *key_ptr);
    llvm::Value *find_or_create_rowset(llvm::Value *key_ptr);
};


class GroupsetAccessor
{
    Jit &jit;
    std::shared_ptr<Accessors> accessors;
    llvm::Value *groupset;

public:
    GroupsetAccessor(Jit &jit, std::shared_ptr<Accessors> accessors, llvm::Value *groupset);

    llvm::Value *find_state(llvm::Value *key_ptr);
    llvm::Value *create_state(llvm::Value *key_ptr);
    llvm::Value *get_state(llvm::Value *index);
    llvm::Value *get_key(llvm::Value *index);
    llvm::Value *get_groupset_size();
};

}    // namespace shdb
