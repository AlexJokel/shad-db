#pragma once

#include "accessors.h"
#include "jit.h"
#include "schema.h"

namespace shdb {

struct Aggregate
{
    Jit &jit;
    Type type;

    Aggregate(Jit &jit, Type type);
    virtual size_t get_state_size() = 0;
    virtual void init(llvm::Value *state_ptr) = 0;
    virtual void update(llvm::Value *state_ptr, const JitValue &value) = 0;
    virtual JitValue finalize(llvm::Value *state_ptr) = 0;
};

struct Sum : public Aggregate
{
    // Your code goes here
};

struct Min : public Aggregate
{
    // Your code goes here
};

struct Max : public Aggregate
{
    // Your code goes here
};

class Avg : public Aggregate
{
    // Your code goes here
};

std::unique_ptr<Aggregate> create_aggregate(Jit &jit, const std::string &name, Type type);

}    // namespace shdb
