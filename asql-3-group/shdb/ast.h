#pragma once

#include "schema.h"

#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace shdb {

enum class AstType {
    name,
    string,
    number,
    binary,
    unary,
    list,
    function,
    order,
    select,
    insert,
    create,
    drop,
};

enum class Opcode {
    plus,
    minus,
    mul,
    div,
    eq,
    ne,
    lt,
    le,
    gt,
    ge,
    land,
    lor,
    lnot,
    uminus,
};


struct Ast
{
    AstType type;

    Ast(AstType type);
    virtual ~Ast() = default;
};

struct String : public Ast
{
    std::string value;

    String(AstType type, std::string value);
};

struct Number : public Ast
{
    int value;

    Number(int value);
};

struct BinaryOp : public Ast
{
    Opcode op;
    std::shared_ptr<Ast> lhs;
    std::shared_ptr<Ast> rhs;

    BinaryOp(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast> rhs);
};

struct UnaryOp : public Ast
{
    Opcode op;
    std::shared_ptr<Ast> operand;

    UnaryOp(Opcode op, std::shared_ptr<Ast> operand);
};

struct List : public Ast
{
    std::vector<std::shared_ptr<Ast>> list;

    List();
    List(std::shared_ptr<Ast> element);
    void append(std::shared_ptr<Ast> element);
};

struct Function : public Ast
{
    std::string name;
    std::shared_ptr<List> args;

    Function(std::string name, std::shared_ptr<List> args);
};

struct Order : public Ast
{
    std::shared_ptr<Ast> expr;
    bool desc;

    Order(std::shared_ptr<Ast> expr, bool desc);
};

struct Select : public Ast
{
    std::shared_ptr<List> list;
    std::vector<std::string> from;
    std::shared_ptr<Ast> where;
    std::shared_ptr<List> group;
    std::shared_ptr<Ast> having;
    std::shared_ptr<List> order;

    Select(
        std::shared_ptr<List> list,
        std::vector<std::string> from,
        std::shared_ptr<Ast> where,
        std::shared_ptr<List> group,
        std::shared_ptr<Ast> having,
        std::shared_ptr<List> order);
};

struct Insert : public Ast
{
    std::string table;
    std::shared_ptr<List> values;

    Insert(std::string table, std::shared_ptr<List> values);
};

struct Create : public Ast
{
    std::string table;
    std::shared_ptr<Schema> schema;

    Create(std::string table, std::shared_ptr<Schema> schema);
};

struct Drop : public Ast
{
    std::string table;

    explicit Drop(std::string table);
};


std::shared_ptr<Ast> new_name(std::string value);
std::shared_ptr<Ast> new_string(std::string value);
std::shared_ptr<Ast> new_number(int value);
std::shared_ptr<Ast> new_binary(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast> rhs);
std::shared_ptr<Ast> new_unary(Opcode op, std::shared_ptr<Ast> operand);
std::shared_ptr<List> new_list();
std::shared_ptr<List> new_list(std::shared_ptr<Ast> element);
std::shared_ptr<Ast> new_function(std::string name, std::shared_ptr<List> args);
std::shared_ptr<Ast> new_order(std::shared_ptr<Ast> expr, bool desc);
std::shared_ptr<Ast> new_select(
    std::shared_ptr<List> list,
    std::vector<std::string> from,
    std::shared_ptr<Ast> where,
    std::shared_ptr<List> group,
    std::shared_ptr<Ast> having,
    std::shared_ptr<List> order);
std::shared_ptr<Ast> new_insert(std::string table, std::shared_ptr<List> values);
std::shared_ptr<Ast> new_create(std::string table, Schema schema);
std::shared_ptr<Ast> new_drop(std::string table);

std::string to_string(std::shared_ptr<Ast> ast);

}    // namespace shdb
