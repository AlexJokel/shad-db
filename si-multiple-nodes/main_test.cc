
#include "retrier.h"

extern void TestStorageSimple();
extern void TestClientTransactionSimple();
extern void TestClientServerTransaction(
  double drop_probability, IRetrier* client_retrier,
  IRetrier* server_retrier);

extern void TestClientServerTwoConflictingTransactions(
  double drop_probability, IRetrier* retrier[4]);

int main() {
  TestClientTransactionSimple();
  TestStorageSimple();
  
  double drop_probability = 0.0;
  
  RetrierNoRetries client_retrier, server_retrier;
  TestClientServerTransaction(
      drop_probability, &client_retrier, &server_retrier);

  RetrierNoRetries rt[4];
  IRetrier* irt[4];

  for (int i = 0; i < 4; i++) {
    irt[i] = &rt[i];
  }

  TestClientServerTwoConflictingTransactions(drop_probability, irt);

  return 0;
}
