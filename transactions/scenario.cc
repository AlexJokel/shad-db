
#include <assert.h>
#include <unordered_map>

#include "client.h"
#include "client_transaction.h"
#include "log.h"
#include "env.h"
#include "scenario.h"
#include "server.h"
#include "storage.h"
#include "types.h"

struct Event {
  bool put;
  Timestamp timestamp;
  Key key;
  Value value;
  TransactionId txid;
};

template <class T1, class T2, class Order, class Equal>
void assert_unorderedly_equal(std::vector<T1> lhs, std::vector<T2> rhs, Order order, Equal equal) {
  std::sort(lhs.begin(), lhs.end(), order);
  std::sort(rhs.begin(), rhs.end(), order);
  assert(lhs.size() == rhs.size());
  for (size_t i = 0; i < lhs.size(); ++i) {
    assert(equal(lhs[i], rhs[i]));
  }
}

bool compare_dump_item(const Storage::DumpItem& u, const Storage::DumpItem& v) {
  return std::make_pair(std::get<0>(u), std::get<1>(u)) <
      std::make_pair(std::get<0>(v), std::get<1>(v));
}

void validate_results(
    const std::vector<KeyInterval>& intervals,
    const std::vector<std::unique_ptr<Server>>& servers,
    const std::vector<ClientTransactionSpec>& specs,
    const std::vector<ClientTransactionState>& states,
    const std::vector<ClientTransactionResults>& all_results) {
  std::vector<Event> events;

  assert(specs.size() == states.size());
  assert(specs.size() == all_results.size());
  for (size_t i = 0; i < specs.size(); ++i) {
    const auto& spec = specs[i];
    const auto state = states[i];
    const auto& results = all_results[i];

    LOG_INFO << "Transaction " << results.txid << " is in \""
             << format_client_transaction_state(state) << "\" state";

    if (spec.action == ClientTransactionSpec::COMMIT) {
      assert(
        state == ClientTransactionState::ROLLED_BACK_BY_SERVER ||
        state == ClientTransactionState::COMMITTED);
    } else if (spec.action == ClientTransactionSpec::ROLLBACK) {
      assert(
        state == ClientTransactionState::ROLLED_BACK ||
        state == ClientTransactionState::ROLLED_BACK_BY_SERVER);
    } else {
      assert(false);
    }

    assert(results.read_timestamp >= spec.earliest_start_timestamp);
    assert(results.read_timestamp != UNDEFINED_TIMESTAMP);
    if (state == ClientTransactionState::COMMITTED) {
      assert(results.commit_timestamp >= spec.earliest_commit_timestamp);
      assert(results.commit_timestamp != UNDEFINED_TIMESTAMP);
    }

    assert_unorderedly_equal(
      spec.gets,
      results.gets,
      [] (const auto& lhs, const auto& rhs) {
        return lhs.key < rhs.key;
      },
      [] (const auto& lhs, const auto& rhs) {
        return lhs.key == rhs.key;
      });

    assert_unorderedly_equal(
      spec.puts,
      results.puts,
      [] (const auto& lhs, const auto& rhs) {
        return std::make_pair(lhs.key, lhs.value) < std::make_pair(rhs.key, rhs.value);
      },
      [] (const auto& lhs, const auto& rhs) {
        return std::make_pair(lhs.key, lhs.value) == std::make_pair(rhs.key, rhs.value);
      });

    for (const auto& get : results.gets) {
      events.push_back(Event{
        /* put */ false,
        results.read_timestamp,
        get.key,
        get.value,
        results.txid});
    }

    if (state == ClientTransactionState::COMMITTED) {
      for (const auto& put : results.puts) {
        events.push_back(Event{
          /* put */ true,
          results.commit_timestamp,
          put.key,
          put.value,
          results.txid});
      }
    }
  }

  sort(events.begin(), events.end(), [] (const Event& lhs, const Event& rhs) {
    return lhs.timestamp < rhs.timestamp
      || (lhs.timestamp == rhs.timestamp && lhs.put && !rhs.put);
  });

  // All puts with the same key must have different timestamps.
  {
    std::unordered_map<Key, TransactionId> keys;

    for (size_t start = 0; start < events.size(); ) {
      size_t limit = start + 1;
      while (limit < events.size() && events[start].timestamp == events[limit].timestamp) {
        ++limit;
      }

      keys.clear();
      for (size_t k = start; k < limit; ++k) {
        if (events[k].put) {
          auto inserted = keys.insert(std::make_pair(events[k].key, events[k].txid));
          if (!inserted.second) {
            LOG_ERROR << "Key " << events[k].key << " is commited twice at timestamp "
                      << events[k].timestamp << " by tx "
                      << inserted.first->second << " and tx " << events[k].txid;
            assert(false);
          }
        }
      }

      start = limit;
    }
  }

  {
    std::unordered_map<Key, Value> values;
    for (const auto& event : events) {
      if (event.put) {
        values[event.key] = event.value;
      } else {
        auto it = values.find(event.key);
        auto expected_value = it == values.end()
          ? DEFAULT_VALUE
          : it->second;
        assert(event.value == expected_value);
      }
    }
  }

  {
    // Validate that the contents of storage is exactly what has been Put.
    size_t interval_puts = 0;
    size_t total_puts = 0;
    
    for (const auto& event : events) {
      if (event.put) {
        total_puts += 1;
      }
    }
    
    for (int i = 0; i < intervals.size(); i++) {
      std::vector<std::tuple<Key, Timestamp, Storage::TagValue>> server_data, event_data;
      servers[i]->storage().dump(&server_data);
      std::sort(server_data.begin(), server_data.end(), compare_dump_item);
      
      for (const auto& event : events) {
        if (event.put) {
          if (event.key >= intervals[i].begin &&
              event.key < intervals[i].end) {
            event_data.push_back(std::make_tuple(event.key, event.timestamp,
                Storage::TagValue{event.txid, event.value}));
          }
        }
      }
      
      std::sort(event_data.begin(), event_data.end(), compare_dump_item);

      interval_puts += event_data.size();
      assert(server_data.size() == event_data.size());

      for (int j = 0; j < server_data.size(); j++) {
        const auto [skey, stimestamp, stv] = server_data[j];
        const auto [ekey, etimestamp, etv] = event_data[j];
        assert(skey == ekey);
        assert(stimestamp == etimestamp);
        assert(stv.value == etv.value);
        assert(stv.txid == etv.txid);
      }
    }
    assert(interval_puts == total_puts);
    LOG_INFO << "Total " << total_puts << " keys have been Put, across "
             << intervals.size() << " intervals.";
  }

  {
    auto get_timestamp_range = [] (const ClientTransactionResults& results) {
      return std::make_pair(
        results.read_timestamp,
        results.commit_timestamp == UNDEFINED_TIMESTAMP
          ? std::numeric_limits<Timestamp>::max()
          : results.commit_timestamp);
    };

    auto are_timestamp_ranges_intersect = [] (
        std::pair<Timestamp,Timestamp> lhs,
        std::pair<Timestamp,Timestamp> rhs) {
      return std::max(lhs.first, rhs.first) <= std::min(lhs.second, rhs.second);
    };

    std::unordered_set<Key> keys;
    for (int i = 0; i < states.size(); ++i) {
      if (states[i] != ClientTransactionState::ROLLED_BACK_BY_SERVER) {
        continue;
      }

      keys.clear();
      for (const auto& put : all_results[i].puts) {
        keys.insert(put.key);
      }

      bool found = false;
      for (int j = 0; j < states.size(); ++j) {
        if (i == j) {
          continue;
        }
        if (!are_timestamp_ranges_intersect(
            get_timestamp_range(all_results[i]),
            get_timestamp_range(all_results[j]))) {
          continue;
        }
        for (const auto& put : all_results[j].puts) {
          if (keys.find(put.key) != keys.end()) {
            found = true;
            break;
          }
        }
      }

      assert(found);
    }
  }
}

void run_scenario(
    const std::string& name,
    const std::vector<KeyInterval>& key_intervals,
    double drop_probability,
    const std::vector<std::vector<ClientTransactionSpec>>& clients_tx_specs) {
  LOG_INFO << "Running scenario \"" << name << "\"";

  Env env(/* mean_delivery_delay */ 10, drop_probability);
  ActorId last_actor_id = 0;

  std::vector<ActorId> server_ids;
  assert(key_intervals.size() > 0);
  
  std::vector<std::unique_ptr<Server>> servers;

  for (size_t i = 0; i < key_intervals.size(); ++i) {
    auto id = last_actor_id++;
    server_ids.push_back(id);
    servers.push_back(std::make_unique<Server>(
      id,
      key_intervals,
      EnvProxy(&env, id)));
    env.register_actor(servers.back().get());
  }

  Discovery discovery(server_ids, key_intervals);

  std::vector<std::unique_ptr<Client>> clients;
  for (const auto& client_tx_specs : clients_tx_specs) {
    auto id = last_actor_id++;
    clients.push_back(std::make_unique<Client>(
      id,
      client_tx_specs,
      &discovery,
      EnvProxy(&env, id)));
    env.register_actor(clients.back().get());
  }

  env.run();

  std::vector<ClientTransactionSpec> specs;
  std::vector<ClientTransactionState> states;
  std::vector<ClientTransactionResults> all_results;
  for (size_t i = 0; i < clients_tx_specs.size(); ++i) {
    for (size_t j = 0; j < clients_tx_specs[i].size(); ++j) {
      const auto& tx = clients[i]->get_tx(j);
      specs.push_back(clients_tx_specs[i][j]);
      states.push_back(tx.state());
      all_results.push_back(tx.export_results());
    }
  }

  LOG_INFO << "Validating scenario \"" << name << "\"";
  LOG_INFO << "Sent " << env.get_sent_message_count() << " messages in total";
  validate_results(key_intervals, servers, specs, states, all_results);

  LOG_INFO << "Scenario \"" << name << "\" is OK ";
}

void run_scenario_simple(
    const std::string& name,
    const std::vector<KeyInterval>& key_intervals,
    double drop_probability) {
  run_scenario(
    name,
    key_intervals,
    drop_probability,
    /* client_tx_specs */ {{
      {
        /* earliest_start_timestamp */ 5,
        /* earliest_commit_timestamp */ 1000,
        /* gets */ {
          {
            /* earliest_timestamp */ 200,
            /* key */ -5
          },
          {
            /* earliest_timestamp */ 200,
            /* key */ 3
          },
          {
            /* earliest_timestamp */ 500,
            /* key */ 3
          }
        },
        /* puts */ {
          {
            /* earliest_timestamp */ 120,
            /* key */ -5,
            /* value */ 42,
          },
          {
            /* earliest_timestamp */ 250,
            /* key */ 3,
            /* value */ 43
          }
        },
        /* action */ ClientTransactionSpec::COMMIT,
      }
    }});
}

void run_scenario_multiple_transactions(
    const std::string& name,
    const std::vector<KeyInterval>& key_intervals,
    double drop_probability) {
  run_scenario(
    name,
    key_intervals,
    drop_probability,
    /* client_tx_specs */ {{
      {
        /* earliest_start_timestamp */ 5,
        /* earliest_commit_timestamp */ 1000,
        /* gets */ {
          {
            /* earliest_timestamp */ 200,
            /* key */ -5
          },
          {
            /* earliest_timestamp */ 200,
            /* key */ 3
          },
          {
            /* earliest_timestamp */ 500,
            /* key */ 3
          }
        },
        /* puts */ {
          {
            /* earliest_timestamp */ 120,
            /* key */ -5,
            /* value */ 42,
          },
          {
            /* earliest_timestamp */ 250,
            /* key */ 3,
            /* value */ 43
          }
        },
        /* action */ ClientTransactionSpec::COMMIT,
      },
      {
        /* earliest_timestamp */ 5,
        /* commit_timestamp */ 1000,
        /* gets */ {
          {
            /* earliest_timestamp */ 200,
            /* key */ -5
          },
          {
            /* earliest_timestamp */ 200,
            /* key */ 3
          },
          {
            /* earliest_timestamp */ 500,
            /* key */ 3
          }
        },
        /* puts */ {
          {
            /* earliest_timestamp */ 120,
            /* key */ -5,
            /* value */ 142,
          },
          {
            /* earliest_timestamp */ 250,
            /* key */ 3,
            /* value */ 143
          }
        },
        /* action */ ClientTransactionSpec::COMMIT,
      }
    },
      {
      {
        /* earliest_start_timestamp */ 5,
        /* earliest_commit_timestamp */ 1000,
        /* gets */ {
          {
            /* earliest_timestamp */ 200,
            /* key */ -5
          },
          {
            /* earliest_timestamp */ 200,
            /* key */ 3
          },
          {
            /* earliest_timestamp */ 500,
            /* key */ 3
          }
        },
        /* puts */ {
          {
            /* earliest_timestamp */ 120,
            /* key */ -5,
            /* value */ 42,
          },
          {
            /* earliest_timestamp */ 250,
            /* key */ 3,
            /* value */ 43
          }
        },
        /* action */ ClientTransactionSpec::COMMIT,
      },
      {
        /* earliest_start_timestamp */ 5,
        /* earliest_commit_timestamp */ 1000,
        /* gets */ {
          {
            /* earliest_timestamp */ 200,
            /* key */ -5
          },
          {
            /* earliest_timestamp */ 200,
            /* key */ 3
          },
          {
            /* earliest_timestamp */ 500,
            /* key */ 3
          }
        },
        /* puts */ {
          {
            /* earliest_timestamp */ 120,
            /* key */ -5,
            /* value */ 242,
          },
          {
            /* earliest_timestamp */ 250,
            /* key */ 3,
            /* value */ 243
          }
        },
        /* action */ ClientTransactionSpec::COMMIT,
      }
    }});
}

void run_scenario_randomized(
    int n_intervals, double drop_probability,
    int n_clients, int n_tx, int n_gets, int n_puts) {
  std::vector<KeyInterval> intervals;
  Key start = 0;
  int interval_len = 10;
  for (int i = 0; i < n_intervals; i++) {
    intervals.push_back({start, start + interval_len});
    start += interval_len;
  }

  std::vector<std::vector<ClientTransactionSpec>> clients_tx_specs(n_clients);

  std::mt19937 gen(4321);
  std::uniform_int_distribution uf100(0, 100);
  std::uniform_int_distribution uf1000(0, 1000);
  std::uniform_int_distribution ufkey(0, n_intervals * interval_len - 1);
  std::uniform_int_distribution ufvalue(0, 10000);

  for (int i = 0; i < n_clients; i++) {
    for (int j = 0; j < n_tx; j++) {
      ClientTransactionSpec spec;
      spec.earliest_start_timestamp = uf100(gen);
      spec.earliest_commit_timestamp = 1000 + uf100(gen);

      spec.action = uf100(gen) < 4 ? ClientTransactionSpec::ROLLBACK :
          ClientTransactionSpec::COMMIT;

      for (int k = 0; k < n_gets; k++) {
        spec.gets.push_back({
          uf1000(gen),
          ufkey(gen)
        });
      }

      for (int k = 0; k < n_puts; k++) {
        spec.puts.push_back({
          uf1000(gen),
          ufkey(gen),
          ufvalue(gen)
        });
      }

      // Only keep Puts with unique keys.
      std::stable_sort(spec.puts.begin(), spec.puts.end(),
          [](ClientTransactionPut u, ClientTransactionPut v) {
              return u.key < v.key;
            });
      spec.puts.erase(std::unique(spec.puts.begin(), spec.puts.end(),
          [](ClientTransactionPut u, ClientTransactionPut v) {
              return u.key == v.key;
            }), spec.puts.end());

      clients_tx_specs[i].push_back(spec);
    }
  }

  run_scenario("randomized", intervals, drop_probability, clients_tx_specs);
}
