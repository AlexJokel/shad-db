#include <cstdio>

#include "message.h"

static int next_id_{0};

static int64_t get_next_id() { return ++next_id_; }

std::string format_message_type(MessageType type) {
  switch (type) {
  case MSG_UNDEFINED:
    return "undefined";
  case MSG_START:
    return "start";
  case MSG_START_ACK:
    return "start_ack";
  case MSG_GET:
    return "get";
  case MSG_GET_REPLY:
    return "get_reply";
  case MSG_PUT:
    return "put";
  case MSG_PUT_REPLY:
    return "put_reply";
  case MSG_ROLLED_BACK_BY_SERVER:
    return "rolled_back_by_server";
  case MSG_PREPARE:
    return "prepare";
  case MSG_PREPARE_ACK:
    return "prepare_ack";
  case MSG_CONFLICT:
    return "conflict";
  case MSG_COMMIT:
    return "commit";
  case MSG_COMMIT_ACK:
    return "commit_ack";
  case MSG_ROLLBACK:
    return "rollback";
  case MSG_ROLLBACK_ACK:
    return "rollback_ack";
  default:
    return std::string("unknown(") + std::to_string(static_cast<int>(type)) +
           std::string(")");
  }
}

Message CreateMessageStart(ActorId source, ActorId destination,
                           TransactionId txid, Timestamp read_timestamp) {
  Message ret;
  ret.type = MSG_START;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessageStartPayload payload;
  payload.read_timestamp = read_timestamp;
  if (txid == UNDEFINED_TRANSACTION_ID) {
    payload.txid = ret.id;
  } else {
    payload.txid = txid;
  }
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessageStartAck(ActorId source, ActorId destination,
                              TransactionId txid, int64_t read_timestamp) {
  Message ret;
  ret.type = MSG_START_ACK;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessageStartAckPayload payload;
  payload.txid = txid;
  payload.read_timestamp = read_timestamp;
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessageGet(ActorId source, ActorId destination,
                         TransactionId txid, Key key) {
  Message ret;
  ret.type = MSG_GET;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessageGetPayload payload;
  payload.txid = txid;
  payload.key = key;
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessagePut(ActorId source, ActorId destination,
                         TransactionId txid, Key key, Value value) {
  Message ret;
  ret.type = MSG_PUT;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessagePutPayload payload;
  payload.txid = txid;
  payload.key = key;
  payload.value = value;
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessageGetReply(ActorId source, ActorId destination,
                              TransactionId txid, int req_id, Value value) {
  Message ret;
  ret.type = MSG_GET_REPLY;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessageGetReplyPayload payload;
  payload.txid = txid;
  payload.request_id = req_id;
  payload.value = value;
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessagePutReply(ActorId source, ActorId destination,
                              TransactionId txid, int req_id) {
  Message ret;
  ret.type = MSG_PUT_REPLY;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessagePutReplyPayload payload;
  payload.txid = txid;
  payload.request_id = req_id;
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessageRolledBackByServer(ActorId source, ActorId destination,
                                        TransactionId txid,
                                        int64_t conflict_txid) {
  Message ret;
  ret.type = MSG_ROLLED_BACK_BY_SERVER;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessageRolledBackByServerPayload payload;
  payload.txid = txid;
  payload.conflict_txid = conflict_txid;
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessagePrepare(ActorId source, ActorId destination,
                             TransactionId txid, Timestamp commit_timestamp) {
  Message ret;
  ret.type = MSG_PREPARE;
  ret.id = get_next_id();
  ret.destination = destination;
  ret.source = source;

  MessagePreparePayload payload;
  payload.txid = txid;
  payload.commit_timestamp = commit_timestamp;
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessagePrepareAck(ActorId source, ActorId destination,
                                TransactionId txid) {
  Message ret;
  ret.type = MSG_PREPARE_ACK;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessagePrepareAckPayload payload;
  payload.txid = txid;
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessageConflict(ActorId source, ActorId destination,
                              TransactionId txid, TransactionId conflict_txid) {
  Message ret;
  ret.type = MSG_CONFLICT;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessageConflictPayload payload;
  payload.txid = txid;
  payload.conflict_txid = conflict_txid;
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessageCommit(ActorId source, ActorId destination,
                            TransactionId txid,
                            std::vector<ActorId> participants) {
  Message ret;
  ret.type = MSG_COMMIT;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessageCommitPayload payload;
  payload.txid = txid;
  payload.participants = std::move(participants);
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessageCommitAck(ActorId source, ActorId destination,
                               TransactionId txid, Timestamp commit_timestamp) {
  Message ret;
  ret.type = MSG_COMMIT_ACK;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessageCommitAckPayload payload;
  payload.txid = txid;
  payload.commit_timestamp = commit_timestamp;
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessageRollback(ActorId source, ActorId destination,
                              TransactionId txid) {
  Message ret;
  ret.type = MSG_ROLLBACK;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessageRollbackPayload payload;
  payload.txid = txid;
  ret.payload = std::move(payload);

  return ret;
}

Message CreateMessageRollbackAck(ActorId source, ActorId destination,
                                 TransactionId txid) {
  Message ret;
  ret.type = MSG_ROLLBACK_ACK;
  ret.id = get_next_id();
  ret.source = source;
  ret.destination = destination;

  MessageRollbackAckPayload payload;
  payload.txid = txid;
  ret.payload = std::move(payload);

  return ret;
}
