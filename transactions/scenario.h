#pragma once

// This library implements testing scenarios.

#include <string>
#include <vector>

#include "discovery.h"

void run_scenario_multiple_transactions(
    const std::string& name,
    const std::vector<KeyInterval>& server_key_intervals,
    double drop_probability);

void run_scenario_simple(
    const std::string& name,
    const std::vector<KeyInterval>& server_key_intervals,
    double drop_probability);

void run_scenario_randomized(
    int n_servers, double drop_probability, int n_clients, int n_tx, int n_gets, int n_puts);
