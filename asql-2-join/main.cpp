#include "shdb/db.h"
#include "shdb/sql.h"

#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <cassert>
#include <chrono>
#include <iostream>
#include <sstream>

auto fixed_schema = std::make_shared<shdb::Schema>(shdb::Schema{{"id", shdb::Type::uint64},
                                                                {"name", shdb::Type::varchar, 1024},
                                                                {"age", shdb::Type::uint64},
                                                                {"graduated", shdb::Type::boolean}});

std::shared_ptr<shdb::Database> create_database(int frame_count)
{
    auto db = shdb::connect("./mydb", frame_count);
    if (db->check_table_exists("test_table")) {
        db->drop_table("test_table");
    }
    db->create_table("test_table", fixed_schema);
    return db;
}

void populate(shdb::Sql &sql)
{
    sql.execute("DROP TABLE test_table");
    sql.execute("CREATE TABLE test_table (id uint64, age uint64, name string, girl boolean)");
    sql.execute("INSERT test_table VALUES (0, 10+10, \"Ann\", 1>0)");
    sql.execute("INSERT test_table VALUES (1, 10+10+1, \"Bob\", 1<0)");
    sql.execute("INSERT test_table VALUES (2, 10+9, \"Sara\", 1>0)");

    sql.execute("DROP TABLE test_orders");
    sql.execute("CREATE TABLE test_orders (id uint64, order string, price uint64)");
    sql.execute("INSERT test_orders VALUES (0, \"pizza\", 99)");
    sql.execute("INSERT test_orders VALUES (0, \"cola\", 49)");
    sql.execute("INSERT test_orders VALUES (2, \"burger\", 599)");
}

void test_join()
{
    auto db = create_database(1);
    auto sql = shdb::Sql(db);
    populate(sql);

    auto rows1 = std::vector<shdb::Row>{{static_cast<uint64_t>(0),
                                         static_cast<uint64_t>(20),
                                         std::string("Ann"),
                                         true,
                                         std::string("pizza"),
                                         static_cast<uint64_t>(99)},
                                        {static_cast<uint64_t>(0),
                                         static_cast<uint64_t>(20),
                                         std::string("Ann"),
                                         true,
                                         std::string("cola"),
                                         static_cast<uint64_t>(49)},
                                        {static_cast<uint64_t>(2),
                                         static_cast<uint64_t>(19),
                                         std::string("Sara"),
                                         true,
                                         std::string("burger"),
                                         static_cast<uint64_t>(599)}};

    auto result1 = sql.execute("SELECT * FROM test_table, test_orders");
    assert_rows_equal(rows1, result1);

    auto rows2 = std::vector<shdb::Row>{{static_cast<uint64_t>(0),
                                         static_cast<uint64_t>(20),
                                         std::string("Ann"),
                                         true,
                                         std::string("pizza"),
                                         static_cast<uint64_t>(99)},
                                        {static_cast<uint64_t>(2),
                                         static_cast<uint64_t>(19),
                                         std::string("Sara"),
                                         true,
                                         std::string("burger"),
                                         static_cast<uint64_t>(599)}};

    auto result2 = sql.execute("SELECT * FROM test_table, test_orders WHERE price > 50");
    assert_rows_equal(rows2, result2);

    auto rows3 = std::vector<shdb::Row>{{std::string("Ann"), static_cast<uint64_t>(99)},
                                        {std::string("Ann"), static_cast<uint64_t>(49)},
                                        {std::string("Sara"), static_cast<uint64_t>(599)}};

    auto result3 = sql.execute("SELECT name, price FROM test_table, test_orders");
    assert_rows_equal(rows3, result3);

    auto rows4 = std::vector<shdb::Row>{{std::string("Ann"), static_cast<uint64_t>(99)},
                                        {std::string("Sara"), static_cast<uint64_t>(599)}};

    auto result4 = sql.execute("SELECT name, price FROM test_table, test_orders WHERE price > 50");
    assert_rows_equal(rows4, result4);

    auto rows5 = std::vector<shdb::Row>{{std::string("Sara"), static_cast<uint64_t>(599)},
                                        {std::string("Ann"), static_cast<uint64_t>(99)}};

    auto result5 = sql.execute("SELECT name, price FROM test_table, test_orders WHERE price > 50 ORDER BY name DESC");
    assert_rows_equal(rows5, result5);

    auto rows6 = std::vector<shdb::Row>{{std::string("Ann"), static_cast<uint64_t>(49)},
                                        {std::string("Ann"), static_cast<uint64_t>(99)},
                                        {std::string("Sara"), static_cast<uint64_t>(599)}};

    auto result6 = sql.execute("SELECT name, price FROM test_table, test_orders ORDER BY price");
    assert_rows_equal(rows6, result6);

    auto rows7 = std::vector<shdb::Row>{{static_cast<uint64_t>(2),
                                         static_cast<uint64_t>(19),
                                         std::string("Sara"),
                                         true,
                                         std::string("burger"),
                                         static_cast<uint64_t>(599)},
                                        {static_cast<uint64_t>(0),
                                         static_cast<uint64_t>(20),
                                         std::string("Ann"),
                                         true,
                                         std::string("cola"),
                                         static_cast<uint64_t>(49)},
                                        {static_cast<uint64_t>(0),
                                         static_cast<uint64_t>(20),
                                         std::string("Ann"),
                                         true,
                                         std::string("pizza"),
                                         static_cast<uint64_t>(99)}};

    auto result7 = sql.execute("SELECT * FROM test_table, test_orders ORDER BY name DESC, price");
    assert_rows_equal(rows7, result7);

    std::cout << "Test join passed" << std::endl;
}

void cmd()
{
    auto db = shdb::connect("./mydb", 1);
    auto sql = shdb::Sql(db);

    while (!std::cin.eof()) {
        std::cout << "shdb> " << std::flush;
        std::string line;
        std::getline(std::cin, line);
        if (!line.empty()) {
            try {
                auto rowset = sql.execute(line);
                for (auto *row : rowset.rows) {
                    std::cout << to_string(*row) << std::endl;
                }
            } catch (char const *ex) {
                std::cout << "Error: " << ex << std::endl;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    test_join();
}
