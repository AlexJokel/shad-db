#include "accessors.h"

#include "row.h"
#include "rowset.h"

#include <cassert>

namespace shdb {

Row *rowset_allocate(Rowset *rowset)
{
    return rowset->allocate();
}

Row *rowset_get(Rowset *rowset, uint64_t index)
{
    return rowset->rows[index];
}

uint64_t rowset_size(Rowset *rowset)
{
    return rowset->rows.size();
}

void rowset_sort(Rowset *rowset, int (*comparer)(const Row *lhs, const Row *rhs))
{
    return rowset->sort_rows(comparer);
}

bool row_load_boolean(Row *row, int index)
{
    return std::get<bool>((*row)[index]);
}

uint64_t row_load_uint64(Row *row, int index)
{
    return std::get<uint64_t>((*row)[index]);
}

const char *row_load_varchar(Row *row, int index)
{
    return std::get<std::string>((*row)[index]).c_str();
}

const char *row_load_string(Row *row, int index)
{
    return std::get<std::string>((*row)[index]).c_str();
}

uint64_t row_load_string_length(Row *row, int index)
{
    return std::get<std::string>((*row)[index]).size();
}

void row_store_boolean(Row *row, int index, bool value)
{
    (*row)[index] = value;
}

void row_store_uint64(Row *row, int index, uint64_t value)
{
    (*row)[index] = value;
}

void row_store_varchar(Row *row, int index, char *str, uint64_t length)
{
    (*row)[index] = std::string(str, length);
}

void row_store_string(Row *row, int index, char *str, uint64_t length)
{
    (*row)[index] = std::string(str, length);
}

int compare_strings(char *lhs_str, uint64_t lhs_length, char *rhs_str, uint64_t rhs_length)
{
    auto lhs = std::string_view(lhs_str, lhs_length);
    auto rhs = std::string_view(rhs_str, rhs_length);
    int result = lhs.compare(rhs);
    return result < 0 ? -1 : (result > 0 ? 1 : 0);
}


Accessors::Accessors(Jit &jit)
{
    link_accessors(jit);
    declare_accessors(jit);
}

void Accessors::link_accessors(Jit &jit)
{
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::rowset_allocate), "rowset_allocate");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::rowset_get), "rowset_get");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::rowset_size), "rowset_size");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::rowset_sort), "rowset_sort");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::row_load_boolean), "row_load_boolean");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::row_load_uint64), "row_load_uint64");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::row_load_varchar), "row_load_varchar");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::row_load_string), "row_load_string");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::row_load_string_length), "row_load_string_length");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::row_store_boolean), "row_store_boolean");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::row_store_uint64), "row_store_uint64");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::row_store_varchar), "row_store_varchar");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::row_store_string), "row_store_string");
    jit.register_symbol(reinterpret_cast<uintptr_t>(shdb::compare_strings), "compare_strings");
}

void Accessors::declare_accessors(Jit &jit)
{
    rowset_allocate = jit.create_function("rowset_allocate", jit.i8ptr_type, {jit.i8ptr_type});

    rowset_get = jit.create_function("rowset_get", jit.i8ptr_type, {jit.i8ptr_type, jit.i64_type});

    rowset_size = jit.create_function("rowset_size", jit.i64_type, {jit.i8ptr_type});

    rowset_sort = jit.create_function("rowset_sort", jit.void_type, {jit.i8ptr_type, jit.i8ptr_type});

    row_load_boolean = jit.create_function("row_load_boolean", jit.i8_type, {jit.i8ptr_type, jit.i32_type});

    row_load_uint64 = jit.create_function("row_load_uint64", jit.i64_type, {jit.i8ptr_type, jit.i32_type});

    row_load_varchar = jit.create_function("row_load_varchar", jit.i8ptr_type, {jit.i8ptr_type, jit.i32_type});

    row_load_string = jit.create_function("row_load_string", jit.i8ptr_type, {jit.i8ptr_type, jit.i32_type});

    row_load_string_length =
        jit.create_function("row_load_string_length", jit.i64_type, {jit.i8ptr_type, jit.i32_type});

    row_store_boolean =
        jit.create_function("row_store_boolean", jit.void_type, {jit.i8ptr_type, jit.i32_type, jit.i8_type});

    row_store_uint64 =
        jit.create_function("row_store_uint64", jit.void_type, {jit.i8ptr_type, jit.i32_type, jit.i64_type});

    row_store_varchar = jit.create_function(
        "row_store_varchar", jit.void_type, {jit.i8ptr_type, jit.i32_type, jit.i8ptr_type, jit.i64_type});

    row_store_string = jit.create_function(
        "row_store_string", jit.void_type, {jit.i8ptr_type, jit.i32_type, jit.i8ptr_type, jit.i64_type});

    compare_strings = jit.create_function(
        "compare_strings", jit.i32_type, {jit.i8ptr_type, jit.i64_type, jit.i8ptr_type, jit.i64_type});
}


RowsetAccessor::RowsetAccessor(Jit &jit, std::shared_ptr<Accessors> accessors, llvm::Value *rowset)
    : jit(jit), accessors(std::move(accessors)), rowset(rowset)
{}

llvm::Value *RowsetAccessor::allocate_row()
{
    return jit.builder.CreateCall(accessors->rowset_allocate, {rowset});
}

llvm::Value *RowsetAccessor::get_row(llvm::Value *index)
{
    return jit.builder.CreateCall(accessors->rowset_get, {rowset, index});
}

llvm::Value *RowsetAccessor::get_rowset_size()
{
    return jit.builder.CreateCall(accessors->rowset_size, {rowset});
}

void RowsetAccessor::sort_rows(llvm::Value *comparer)
{
    jit.builder.CreateCall(accessors->rowset_sort, {rowset, comparer});
}


RowsAccessor::RowsAccessor(Jit &jit, std::shared_ptr<Accessors> accessors, std::shared_ptr<Schema> schema)
    : jit(jit), accessors(std::move(accessors)), schema(std::move(schema))
{}

JitValue RowsAccessor::load_value(llvm::Value *row, int index)
{
    auto *jindex = jit.create_constant(32, index);
    switch ((*schema)[index].type) {
    case Type::boolean: {
        auto *value = jit.builder.CreateCall(accessors->row_load_boolean, {row, jindex});
        return {value, nullptr};
    }
    case Type::uint64: {
        auto *value = jit.builder.CreateCall(accessors->row_load_uint64, {row, jindex});
        return {value, nullptr};
    }
    case Type::varchar: {
        auto *pointer = jit.builder.CreateCall(accessors->row_load_varchar, {row, jindex});
        auto *length = jit.create_constant(64, (*schema)[index].length);
        return {pointer, length};
    }
    case Type::string: {
        auto *pointer = jit.builder.CreateCall(accessors->row_load_string, {row, jindex});
        auto *length = jit.builder.CreateCall(accessors->row_load_string_length, {row, jindex});
        return {pointer, length};
    }
    default: {
        assert(0);
    }
    }
}

void RowsAccessor::store_value(const JitRow &jitrow, llvm::Value *row, int index)
{
    auto *jindex = jit.create_constant(32, index);
    switch ((*schema)[index].type) {
    case Type::boolean: {
        jit.builder.CreateCall(accessors->row_store_boolean, {row, jindex, jitrow[index].first});
        break;
    }
    case Type::uint64: {
        jit.builder.CreateCall(accessors->row_store_uint64, {row, jindex, jitrow[index].first});
        break;
    }
    case Type::varchar: {
        jit.builder.CreateCall(accessors->row_store_varchar, {row, jindex, jitrow[index].first, jitrow[index].second});
        break;
    }
    case Type::string: {
        jit.builder.CreateCall(accessors->row_store_string, {row, jindex, jitrow[index].first, jitrow[index].second});
        break;
    }
    default: {
        assert(0);
    }
    }
}

JitRow RowsAccessor::load_row(llvm::Value *row)
{
    JitRow jitrow;
    for (size_t index = 0; index < schema->size(); ++index) {
        jitrow.push_back(load_value(row, index));
    }
    return jitrow;
}

void RowsAccessor::store_row(const JitRow &jitrow, llvm::Value *row)
{
    for (size_t index = 0; index < schema->size(); ++index) {
        store_value(jitrow, row, index);
    }
}


SchemaAccessor::SchemaAccessor(std::shared_ptr<Schema> schema) : schema(std::move(schema))
{
    if (SchemaAccessor::schema) {
        size_t index = 0;
        for (const auto &column : *SchemaAccessor::schema) {
            mapping[column.name] = index++;
        }
    }
}

bool SchemaAccessor::has(const std::string &name)
{
    return mapping.find(name) != mapping.end();
}

size_t SchemaAccessor::get_index(const std::string &name)
{
    assert(has(name));
    return mapping[name];
}

const ColumnSchema &SchemaAccessor::get_column(const std::string &name)
{
    assert(has(name));
    return (*schema)[mapping[name]];
}

}    // namespace shdb
